using System;
using System.Collections.Generic;
using System.Web;
using UnityEngine;
using Random = UnityEngine.Random;
using System.Linq;

[Serializable]
public class ArticleJsonResult {

    public Article article;
    public string[] links;
    public string[] linksDecoded;

    public string RandomLink() {
        if (links.Length == 0){
            Debug.LogError("LINKS not present");
            return "";
        }
        string link = links[Random.Range(0, links.Length)];
        Crop(link);
        return link;
    }

    void Crop(string protectedLink){
        bool protectedAdded = false;
        int toPlaceAt = Random.Range(10, 54);
        if (links.Length > 60){
            List<string> newLinks = new List<string>();
            for (int i = 0; i < 55; i++){
                if (!protectedAdded){
                    if (i == toPlaceAt){
                        newLinks.Add(protectedLink);
                        protectedAdded = true;
                        continue;
                    }
                    else if (links[i] == protectedLink){
                        protectedAdded = true;
                    }
                }
                newLinks.Add(links[i]);
            }
            this.links = newLinks.ToArray();
            DecodeAll();
        }
    }

    public void DecodeAll() {
        HashSet<string> linksSet = new HashSet<string>(links);
        links = linksSet.ToArray();
        linksDecoded = new string[links.Length];
        for (int i = 0; i < links.Length; i++) {
            linksDecoded[i] = HttpUtility.UrlDecode(links[i]);
        }
        article.name = HttpUtility.UrlDecode(article.name);
        article.description = HttpUtility.UrlDecode(article.description);
    }
}

[Serializable]
public class Article {

    public string description;
    public string image;
    public string name;
    public string url;

}