﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class GetArticles : MonoBehaviour {

    public static GetArticles instance;

    [SerializeField] TextMeshProUGUI startingArticleValue;
    [SerializeField] TextMeshProUGUI targetArticleValue;
    [SerializeField] LinkList linkList;

    ArticleJsonResult startingArticleResult;
    ArticleJsonResult targetArticleResult;
    Coroutine articleGetter = null;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
        if (instance != this) {
            Destroy(this.gameObject);
            return;
        }
    }

    void Start() {
        RunGetStartingArticle();
    }

    public void RunGetStartingArticle() {
        linkList.Clean();
        GameSummary.instance.CleanFields();
        if (articleGetter != null) {
            StopCoroutine(articleGetter);
        }
        articleGetter = StartCoroutine(GetStartingArticle());
    }

    bool foundStarting = false;
    bool foundTarget = false;

    IEnumerator StartingAnimation() {
        int dots = 1;
        string searchingText = "Searching";
        while (!foundStarting || !foundTarget) {
            searchingText = "Searching";
            for (int i = 0; i < dots; i++) {
                searchingText += ".";
            }
            if (!foundStarting) {
                startingArticleValue.text = searchingText;
            }
            if (!foundTarget) {
                targetArticleValue.text = searchingText;
            }
            yield return new WaitForSeconds(0.5f);
            dots = (dots % 3) + 1;
        }
    }

    IEnumerator GetStartingArticle() {
        foundStarting = false;
        foundTarget = false;
        StartCoroutine(StartingAnimation());
        UnityWebRequest startinArticleRequest = UnityWebRequest.Get("https://wiki-api-us.herokuapp.com/random-article");
        yield return startinArticleRequest.SendWebRequest();
        if (startinArticleRequest.isNetworkError || startinArticleRequest.isHttpError) {
            Debug.LogError("Connection error: " + startinArticleRequest.error);
            yield break;
        }
        string jsonText = startinArticleRequest.downloadHandler.text;
        startingArticleResult = JsonUtility.FromJson<ArticleJsonResult>(jsonText);
        foundStarting = true;
        startingArticleValue.text = startingArticleResult.article.name;
        startingArticleResult.DecodeAll();

        UnityWebRequest targetRequest;
        targetArticleResult = startingArticleResult;
        for (int i = 0; i < 2; i++) {
            string randomLink = targetArticleResult.RandomLink();
            targetRequest = UnityWebRequest.Get("https://wiki-api-us.herokuapp.com/article-from-title?title=" + randomLink);
            yield return targetRequest.SendWebRequest();
            if (targetRequest.isNetworkError || targetRequest.isHttpError) {
                Debug.LogError("Connection error: " + targetRequest.error);
                yield break;
            }
            targetArticleResult = JsonUtility.FromJson<ArticleJsonResult>(targetRequest.downloadHandler.text);
        }
        targetArticleResult.DecodeAll();
        foundTarget = true;
        targetArticleValue.text = targetArticleResult.article.name;

        SetLinks();
        GameSummary.instance.InitNewRound();
    }

    public void SetNextArticle(string link) {
        StopAllCoroutines();
        string decoded = HttpUtility.UrlDecode(link).Replace("_", " ");
        linkList.Clean();
        GameSummary.instance.NextMove();
        Debug.Log("COMPARING >>" + decoded + "<< with >>" + targetArticleResult.article.name +"<<");
        if (decoded.Equals(targetArticleResult.article.name)) {
            GameSummary.instance.Win();
            return;
        }
        StartCoroutine(LoadNewArticle(link));
    }

    IEnumerator LoadNewArticle(string link) {
        foundStarting = false;
        StartCoroutine(StartingAnimation());
        var request = UnityWebRequest.Get("https://wiki-api-us.herokuapp.com/article-from-title?title=" + link);
        yield return request.SendWebRequest();
        var newArticleJsonResult = JsonUtility.FromJson<ArticleJsonResult>(request.downloadHandler.text);
        foundStarting = true;
        newArticleJsonResult.DecodeAll();
        startingArticleValue.text = newArticleJsonResult.article.name;
        startingArticleResult = newArticleJsonResult;

        SetLinks();
    }

    void SetLinks() {
        linkList.GenerateLinks(startingArticleResult.links);
    }

}