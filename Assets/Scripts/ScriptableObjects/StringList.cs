using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "StringList", menuName = "ListData/StringList")]
public class StringList : ScriptableObject {

    public List<string> Elements { get { return elements; } }

    [SerializeField] List<string> elements;

}