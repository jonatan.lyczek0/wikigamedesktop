﻿using System.Collections;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using AngleSharp.Html.Dom;

public class OpenFirstArticle : MonoBehaviour {

    [SerializeField] StringList hrefValueInclusion;
    [SerializeField] StringList hrefValueExclusion;

    void Start() {
        // OpenLink("https://pl.wikipedia.org/wiki/Plac_Muranowski_w_Warszawie");
    }

    void OpenLink(string link) {
        UnityWebRequest webRequest = UnityWebRequest.Get(link);
        StartCoroutine(Request(webRequest));
    }

    IEnumerator Request(UnityWebRequest webRequest) {
        yield return webRequest.SendWebRequest();
        if (webRequest.isNetworkError || webRequest.isHttpError) {
            Debug.Log(webRequest.error);
        }
        else {
            // Show results as text
            Debug.Log(webRequest.downloadHandler.text);
            HtmlParser parser = new HtmlParser();
            var document = parser.ParseDocument(webRequest.downloadHandler.text);
            ScrapeIt(document);

        }
    }

    void ScrapeIt(IHtmlDocument document) {
        var links = GetLinks(document);
        foreach (var result in links) {
            string text = result.TextContent;

            // text = Regex.Replace(text, "\\[\\d+]", "");
            Debug.Log(text);
            Debug.Log(result.Attributes.First().Value);
        }
    }

    IEnumerable<IElement> GetLinks(IHtmlDocument document) {
        var allLinks = document.QuerySelectorAll("a");
        var allHrefs = allLinks.Where(a => a.Attributes.All(atr => atr.Name == "href"));

        allHrefs = allLinks.Where(a => {
            var hrefAttr = a.Attributes.GetNamedItem("href");
            if (hrefAttr == null)
                return false;
            
            if (hrefValueInclusion.Elements.Any(element => !hrefAttr.Value.Contains(element)))
                return false;

            if (hrefValueExclusion.Elements.Any(element => hrefAttr.Value.Contains(element)))
                return false;

            return true;
        });
        return allHrefs;
    }

}