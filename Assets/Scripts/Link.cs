using System.Web;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Link : MonoBehaviour {

    public Button Button { get; private set; }
    public TextMeshProUGUI Text { get; private set; }

    void Awake() {
        Text = GetComponentInChildren<TextMeshProUGUI>();
        Button = GetComponent<Button>();
    }

    public void Set(string linkUrl) {
        Text.text = HttpUtility.UrlDecode(linkUrl).Replace("_", " ");
        Button.onClick.AddListener(() => { 
            GetArticles.instance.SetNextArticle(linkUrl);
        });

    }

    public void Flush(){
        Button.onClick.RemoveAllListeners();
    }

}