﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LinkList : MonoBehaviour {

    [SerializeField] GameObject linkPrefab;

    List<Link> spawnedLinks = new List<Link>();
    ScrollRect scrollRect;

    void Awake() {
        scrollRect = GetComponent<ScrollRect>();
    }

    public void GenerateLinks(string[] linksUrls) {
        StartCoroutine(Generate(linksUrls));
    }

    IEnumerator Generate(string[] linksUrls) {
        Debug.LogWarning("Spawning that much links: " + linksUrls.Length);
        float delay = 1f / (float) linksUrls.Length;
        foreach (var url in linksUrls) {
            var linkGO = Instantiate(linkPrefab);
            linkGO.transform.SetParent(scrollRect.content);
            Link link = linkGO.GetComponent<Link>();
            link.Set(url);
            spawnedLinks.Add(link);
            yield return new WaitForSeconds(delay);
        }
    }

    public void Clean() {
        StopAllCoroutines();
        foreach (Link link in spawnedLinks) {
            link.Flush();
            Destroy(link.gameObject);
        }
        spawnedLinks.Clear();
    }

}