﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameSummary : MonoBehaviour {

    public static GameSummary instance = null;

    [SerializeField] Color winColor;
    [SerializeField] Color loseColor;

    [SerializeField] GameObject summary;
    [SerializeField] GameObject movesGroup;
    [SerializeField] GameObject timeGroup;
    [SerializeField] TextMeshProUGUI movesText;
    [SerializeField] TextMeshProUGUI timeText;
    [SerializeField] Image summaryImage;
    [SerializeField] Button playAgainButton;

    float startingTime;
    float endTime;
    int totalMoves = 0;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
        if (instance != this) {
            Destroy(this.gameObject);
            return;
        }
        playAgainButton.onClick.AddListener(() => { GetArticles.instance.RunGetStartingArticle(); });
    }

    public void CleanFields() {
        movesText.text = "";
        timeText.text = "";
        movesGroup.SetActive(false);
        timeGroup.SetActive(false);
        playAgainButton.gameObject.SetActive(false);
        summary.SetActive(false);
    }

    public void InitNewRound() {
        totalMoves = 0;
        startingTime = Time.time;
    }

    public void NextMove() {
        totalMoves++;
    }

    public void Win() {
        summary.SetActive(true);
        summaryImage.color = winColor;
        endTime = Time.time;
        StartCoroutine(SummaryAnimation());
    }

    IEnumerator SummaryAnimation() {
        movesGroup.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        int i = 0;
        while (i != totalMoves) {
            movesText.text = i.ToString();
            yield return new WaitForSeconds(0.2f);
            i++;
        }
        timeGroup.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        float t = 0;
        float target = endTime - startingTime;
        float secretT = 0;
        int m = 0;
        string strS = "00";
        string strM = "00";
        string strMS = "000";
        while (t < target) {
            if (secretT < 10) {
                strS = "0" + Mathf.FloorToInt(secretT).ToString();
            }
            else {
                strS = Mathf.FloorToInt(secretT).ToString();
            }
            if (m < 10) {
                strM = "0" + m.ToString();
            }
            else {
                strM = m.ToString();
            }
            float remainder = secretT % 1f;
            Debug.Log(remainder);
            int remainderI = Mathf.FloorToInt(remainder * 1000);
            if (remainderI < 100f){
                strMS = "0" + remainderI.ToString();
            }
            else if(remainderI < 10f){
                strMS = "00" + remainderI.ToString();
            }
            else{
                strMS = remainderI.ToString();
            }

            timeText.text = strM+ ":" + strS  + ":" + strMS;

            yield return new WaitForSeconds(0.00001f);
            t += 0.2f;
            secretT += 0.2f;
            if (secretT >= 60) {
                m++;
                secretT = 0;
            }
        }
        playAgainButton.gameObject.SetActive(true);

    }

}