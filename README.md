# WikiGameDesktop

Projekt na programowanie zespołowe.
Desktopowa wersja aplikacji wiki game

**Do zaimplementowania**
*  Poziom trudności (poprzez ilość losowań do wybrania następnego artykułu oraz ograniczanie listy linków na artykuł jako opcji wyboru drogi)
*  Przycisk 'More'
*  Przegrana gry?
*  Restart gry

**Opis techniczny projektu**

Aplikacja tworzona w oparciu o silnik Unity3D 2019.12f z użyciem języka C#.

W celu łączenia się z nasza aplikacja (https://wiki-api-us.herokuapp.com/)  z poziomu Unity wykorzystujemy moduł UnityWebRequest zaimplementowany w Unity.Networking.
Pozwala na wysłanie zapytania i otrzymania odpowiedzi w postaci tekstu z naszego serwera.
Do aplikacji wykorzystujemy dwa osobne zapytania
https://wiki-api-us.herokuapp.com/random-article do otrzymania losowego artykułu startowego
oraz 
https://wiki-api-us.herokuapp.com/article-from-title?title=(Tytuł artykułu jako zmienna)

Serwer pod wskazane zapytania zwraca JSONa o strukturze:
;;;
{    "article":{
        "description":"opis artykulu",
        "name": "tytul artykułu",
        "url": "oryginalny link do artykułu na wikipedi",
        "image": "zdjęcie do artykułu jeśli istnieje"
    },
    "links": [tablica linków do których odwołuje się artykuł]
}
;;;

Aby zgrupować dane w tym Jsonie utworzono dwie klasy
Klasa Główna
ArticleJsonResult{
    Article article,
    string[] links
}

oraz 

Article{
    string description,\n
    string name,
    sring url,
    string image
}
gdzie pola klas odpowiadają kluczom z JSONa


**Opis klas**

GetArticles.cs



Klasa ta zawiera logike pobierania artykułów i rozpoczynania gry.
Grę uruchamia poprzez funkcje RunGetStartingArticle która wywołuje korutyne pobierająca link startowy, 
w trakcie uruchomiona jest również korutyna animująca tekst w polach tytułów artykułu startowego i docelowego w trakcie ich szukania i pobierania.
Pobierane dane z serwera zawarte w postaci JSONa są konwertowane na obiekt ArticleJsonResult. 
Po pobraniu startowego artykułu losowo wybierany jest link z listy linków do których odwołuje się dany artykuł, po czym z tego artykułu losowo wybierany 
jest następny itd. Ilość wylosowań kontrolowana jest poprzez stałą wpisaną zmienną w pętli, w późniejszym etapie może ułatwić implementowanie poziomu trudności.
Po 'n' losowaniach ostatni artykul jest wybierany jako docelowy. Po tym etapie lista linków dla obecnego (startowego artykułu) jest wypełniana.

ArticleJsonResult.cs
Pojemnik na otrzymane dane z JSONa z serwera. 
Zawiera obiekt Article oraz liste linków. 
Metoda DecodeALL pozwala na odkodowanie znaków specjalnych (jak np %C3), w trakcie usuwa również duplikaty.
Metoda crop pozwala na ograniczenie ilości linków gdy ta jest zbyt duża.
    
Article.cs
Nazwa, opis oraz link do zdjęcia artykułu i link samego artykułu

LinkList.cs
Generator listy linków, pobiera tablice i sekwencyjnie dodaje linki do listy w interfejsie

Link.cs
Konfigurator interaktywnego obiektu, jego nazwy oraz akcji na wybranie tego linka.

GameSummary.cs
Odmierza czas gry oraz ilość ruchów, pozwala na zakończenie gry i wyświetlenie podsumowania oraz uruchomienie następnej poprzez przycisk na oknie podsumowania.




